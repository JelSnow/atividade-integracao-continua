package fatorial;

import org.junit.Assert;
import org.junit.Test;

public class TestCalculoFatorial {

	@Test
	public void testCalcularFatorialZero() {
		//Setup do Teste
		CalculoFatorial c = new CalculoFatorial();
		
		//Dados de Entrada
		int entrada = 0;
		//Retorno Esperado
		int esperado = 1;
		
		//Execu��o do m�todo calcularFatorial() da classe CalculoFatorial
		int retorno = c.calcularFatorial(entrada);
		
		//Compara��o dos valores esperado e retornado
		Assert.assertEquals(esperado, retorno);
	}
	@Test
	public void testCalcularFatorialUm() {
		//Setup do Teste
		CalculoFatorial c = new CalculoFatorial();
		
		//Dados de Entrada
		int entrada = 1;
		//Retorno Esperado
		int esperado = 1;
		
		//Execu��o do m�todo calcularFatorial() da classe CalculoFatorial
		int retorno = c.calcularFatorial(entrada);
		
		//Compara��o dos valores esperado e retornado
		Assert.assertEquals(esperado, retorno);
	}
	
	@Test
	public void testCalcularFatorialCinco() {
		//Setup do Teste
		CalculoFatorial c = new CalculoFatorial();
		
		//Dados de Entrada
		int entrada = 5;
		//Retorno Esperado
		int esperado = 120;
		
		//Execu��o do m�todo calcularFatorial() da classe CalculoFatorial
		int retorno = c.calcularFatorial(entrada);
		
		//Compara��o dos valores esperado e retornado
		Assert.assertEquals(esperado, retorno);
	}

}
